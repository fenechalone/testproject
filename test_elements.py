from pages.elements_page import ElementsPage


class TestElements:
    def test_fill_textbox(self, browser):
        elements_page = ElementsPage(browser)
        elements_page.open_text_box_page()
        elements_page.fill_in_the_texbox_from('test', 'test@gmail.com',
                                           '420088, Респ Татарстан, г Казань, пр-кт Победы, д 320','test'
                                           )
        elements_page.click_submit()
