from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium import webdriver
import math
import time
try:
 browser = webdriver.Chrome()
# browser.implicitly_wait(14)
 browser.get("http://suninjuly.github.io/explicit_wait2.html")
 button = WebDriverWait(browser, 12).until(
     EC.text_to_be_present_in_element((By.ID, "price"), "$100"))
 book = browser.find_element(By.ID, "book")
 book.click()
 x = browser.find_element(By.CSS_SELECTOR, '#input_value')
 x = x.text


 def calc(x):
    return str(math.log(abs(12 * math.sin(int(x)))))


 y = calc(x)
 input1 = browser.find_element(By.TAG_NAME, 'input')
 input1.send_keys(y)
 submit = browser.find_element(By.CSS_SELECTOR, "body > form > div > div > button")
 submit.click()

finally:
 # ожидание чтобы визуально оценить результаты прохождения скрипта
 time.sleep(10)
 # закрываем браузер после всех манипуляций
 browser.quit()