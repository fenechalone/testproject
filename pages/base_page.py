from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By

class BasePage:
    def __init__(self, browser):
        self.browser = browser
        self.base_url = 'https://demoqa.com'

    def find_element(self, locator, time=10):
        return WebDriverWait(self.browser, time).until(EC.presence_of_element_located((By.XPATH, locator)),
                                                       message=f"Can't find element by locator {locator}")

    def click(self, locator, time=30):
        return WebDriverWait(self.browser, time).until((EC.element_to_be_clickable((By.XPATH, locator))))



