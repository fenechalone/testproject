from selenium.webdriver.common.by import By


class CommonLocators:

    def inputs_with_id(self, id):
        return f'//input[@id = "{id}"]'

    def textarea_with_id(self, id):
        return f'//textarea[@id = "{id}"]'

    def button_with_id(self, submit):
        return f'//button[@id = "{submit}"]'

