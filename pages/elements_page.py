from .base_page import BasePage
from .common_locators import CommonLocators
from selenium.webdriver.common.keys import Keys

class ElementsPage(BasePage):

    def open_page(self):
        self.browser.get(self.base_url + '/elements')

    def open_text_box_page(self):
        self.browser.get(self.base_url + '/text-box')

    def fill_in_the_texbox_from(self, username, email, currentaddress, permanentaddress):
        self.find_element(CommonLocators().inputs_with_id('userName')).send_keys(username)
        self.find_element(CommonLocators().inputs_with_id('userEmail')).send_keys(email)
        self.find_element(CommonLocators().textarea_with_id('currentAddress')).send_keys(currentaddress)
        self.find_element(CommonLocators().textarea_with_id('permanentAddress')).send_keys(permanentaddress)

    def click_submit(self):
        submit_button = self.find_element(CommonLocators().button_with_id('submit'))
        submit_button.send_keys(Keys.END)
        submit_button.click()
