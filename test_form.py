from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium import webdriver
import math
import time
from selenium.webdriver.common.keys import Keys
import os
from selenium.webdriver.support.ui import Select
try:
 browser = webdriver.Chrome()
 browser.get('https://demoqa.com/forms')
 #базовые методы
# form_element1 = WebDriverWait(browser,5).until(EC.element_to_be_clickable((By.XPATH,"//*[@id='app']/div/div/div[2]/div/div[2]/div/div[2]/svg")))
 #form_element1 = browser.find_element(By.XPATH,"//*[@id='app']/div/div/div[2]/div/div[2]/div/div[2]/svg")
 #form_element1.click()
 form_element2 = browser.find_element(By.XPATH,"//div[@class='element-list collapse show']")
 form_element2.click()
 input1 = browser.find_element(By.ID,'firstName')
 input1.send_keys('Max')
 input2 = browser.find_element(By.ID, 'lastName')
 input2.send_keys('Sokolov')
 email = browser.find_element(By.XPATH, '//*[@id="userEmail"]')
 email.send_keys('fenechalone@gmail.com')
 radiobutton1 = WebDriverWait(browser,5).until(EC.element_to_be_clickable((By.CSS_SELECTOR, '#genterWrapper > div.col-md-9.col-sm-12 > div:nth-child(1) > label')))
 #radiobutton1 = browser.find_element(By.CSS_SELECTOR, '#genterWrapper > div.col-md-9.col-sm-12 > div:nth-child(1) > label')
 radiobutton1.click()
 input3 = browser.find_element(By.ID, 'userNumber')
 input3.send_keys('9872697231')
 calendar = browser.find_element(By.ID, 'dateOfBirthInput')
 #calendar.send_keys(Keys.CONTROL + "a")
 i = 0
 while i <= 9:
     calendar.send_keys(Keys.BACK_SPACE)
     i+=1
     continue
 calendar.send_keys('9 Nov 1996')
 calendar.send_keys(Keys.ENTER)
 subject = browser.find_element(By.XPATH, '//*[@id="subjectsInput"]')
 subject.send_keys('arts')
 subject.send_keys(Keys.ENTER)
 checkbox1 = browser.find_element(By.CSS_SELECTOR,'#hobbiesWrapper > div.col-md-9.col-sm-12 > div:nth-child(2) > label')
 checkbox1.click()
 file = browser.find_element(By.CSS_SELECTOR, "#uploadPicture")
 current_dir = os.path.abspath(os.path.dirname(__file__))  # получаем путь к директории текущего исполняемого файла
 file_path = os.path.join(current_dir, 'file.txt')  # добавляем к этому пути имя файла
 file.send_keys(file_path)
 textarea = browser.find_element(By.XPATH, '//*[@id="currentAddress"]')
 textarea.send_keys('Test')
 State = browser.find_element(By.XPATH,'//*[@id="react-select-3-input"]')
 State.send_keys('NCR')
 State.send_keys(Keys.ENTER)
 City = browser.find_element(By.XPATH,'//*[@id="react-select-4-input"]')
 City.send_keys('Delhi')
 City.send_keys(Keys.ENTER)
 City.send_keys(Keys.ENTER)
 #Submit_form = browser.find_element(By.ID,'submit').click()

finally:
 # ожидание чтобы визуально оценить результаты прохождения скрипта
 time.sleep(10)
 # закрываем браузер после всех манипуляций
 browser.quit()