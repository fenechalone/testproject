from selenium import webdriver
from selenium.webdriver.common.by import By
import time
import math
from selenium.webdriver.support.ui import Select
try:
    link = "https://suninjuly.github.io/selects1.html"
    browser = webdriver.Chrome()
    browser.get(link)
    x1 = browser.find_element(By.CSS_SELECTOR, '#num1')
    y1 = browser.find_element(By.CSS_SELECTOR, '#num2')
    x1 = x1.text
    y1 = y1.text
    def calc(x,y):
     return str(str(int(x)+int(y)))
#    z = int(browser.find_element_by_id("num1").text) + int(browser.find_element_by_id("num2").text)
    z = calc(x1,y1)
    select = Select(browser.find_element(By.TAG_NAME, "select"))
    select.select_by_value(z)  # ищем элемент с текстом "Python"
#    select.select_by_visible_text(z)
    # Проверяем, что смогли зарегистрироваться
    # ждем загрузки страницы
    time.sleep(1)

    submit = browser.find_element(By.CSS_SELECTOR, "body > div > form > button")
    submit.click()
finally:
    # ожидание чтобы визуально оценить результаты прохождения скрипта
    time.sleep(10)
    # закрываем браузер после всех манипуляций
    browser.quit()