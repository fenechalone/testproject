from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium import webdriver
import math
import time
from selenium.webdriver.common.keys import Keys
import os
from selenium.webdriver.support.ui import Select
try:
 browser = webdriver.Chrome()
 browser.get('https://demoqa.com/browser-windows')
 #тест с окнами
 NewTab = browser.find_element(By.XPATH,'//*[@id="tabButton"]').click()
 first_window = browser.window_handles[0]
 New_Window = browser.window_handles[1]
 browser.switch_to.window(New_Window)
 #browser.switch_to.window(NewTab)
 browser.close()
 browser.switch_to.window(first_window)
 NewWindow = browser.find_element(By.XPATH, '// *[ @ id = "windowButton"]').click()
 New_Window2 = browser.window_handles[1]
 browser.switch_to.window(New_Window2)
 browser.close()
 browser.switch_to.window(first_window)
 #alerts
 #NewTabAlerts = browser.find_element(By.CSS_SELECTOR,'#item-1').click()
 browser.get('https://demoqa.com/alerts')
 Alertbutton1 = browser.find_element(By.XPATH,'//*[@id="alertButton"]').click()
 alert = browser.switch_to.alert
 alert.accept()
 Alertbutton2 = browser.find_element(By.XPATH,'// *[ @ id = "timerAlertButton"]').click()
 alert = WebDriverWait(browser,10).until(EC.alert_is_present())
 alert.accept()
 Alertbutton3 = browser.find_element(By.XPATH,'//button[@id = "confirmButton"] ').click()
 confirm = browser.switch_to.alert
 confirm.accept()
 Alertbutton4 = browser.find_element(By.XPATH, '//button[@id = "promtButton"] ').click()
 promt = browser.switch_to.alert
 promt.send_keys('test')
 promt.accept()
 #123457
finally:
 # ожидание чтобы визуально оценить результаты прохождения скрипта
 time.sleep(10)
 # закрываем браузер после всех манипуляций
 browser.quit()